import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginPage } from "./pages/login/login.page";
import { CataloguePage } from "./pages/catalogue/catalogue.page";
import { AuthGuard } from "./services/auth.guard";

const routes: Routes = [
	{
		path: '',
		pathMatch: 'full',
		redirectTo: '/login'
	},
	{
		path: 'login',
		component: LoginPage
	},
	{
		path: 'catalogue',
		component: CataloguePage,
		canActivate: [ AuthGuard ]
	}
]

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
