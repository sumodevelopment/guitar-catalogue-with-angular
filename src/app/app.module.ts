import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { ContainerComponent } from "./components/container.component";
import { LoginPage } from "./pages/login/login.page";
import { CataloguePage } from "./pages/catalogue/catalogue.page";
import { AppRoutingModule } from "./app-routing.module";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

@NgModule({
	declarations: [
		AppComponent,
		ContainerComponent,
		LoginPage,
		CataloguePage
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		FormsModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
