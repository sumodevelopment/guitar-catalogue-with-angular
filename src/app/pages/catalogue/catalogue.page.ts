import { Component } from "@angular/core";
import { SessionService } from "../../services/session.service";
import { User } from "../../models/user.model";

@Component({
	selector: 'app-catalogue-page',
	templateUrl: './catalogue.page.html'
})
export class CataloguePage {
	constructor(private readonly sessionService: SessionService) {
	}

	get user(): User | undefined {
		return this.sessionService.user
	}
}
