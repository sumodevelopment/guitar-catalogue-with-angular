import { Component, OnInit } from "@angular/core";
import { UserService } from "../../services/user.service";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { SessionService } from "../../services/session.service";

@Component({
	selector: 'app-login-page',
	templateUrl: './login.page.html'
})
export class LoginPage implements OnInit {
	constructor(
		private readonly router: Router,
		private readonly userService: UserService,
		private readonly sessionService: SessionService,
	) {
	}

	ngOnInit() {
		if (this.sessionService.user !== undefined) {
			this.router.navigate(['catalogue'])
		}
	}

	get attempting(): boolean {
		return this.userService.attempting;
	}

	onSubmit(loginForm: NgForm): void {

		const { username } = loginForm.value
		this.userService.authenticate(username, async () => {
			await this.router.navigate(['catalogue'])
		})
	}

}
